import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/auth/entities/auth.entity';
import { GetUser } from 'src/auth/get-user.decorator';
import { CreateTaskDto } from './create_task.dto';
import { FilterTaskDto } from './filter_tasks.dto';
import { TasksService } from './tasks.service';
import { UpdateStatusDto } from './update-tasks.dto';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
  private logger = new Logger('TasksController');
  constructor(private tasksService: TasksService) {}
  @Get()
  getTasks(@Query() filterDto: FilterTaskDto, @GetUser() user: User) {
    this.logger.verbose(
      `User "${user.username}" retrieving all tasks. Filters: ${JSON.stringify(
        filterDto,
      )}`,
    );
    return this.tasksService.getTasks(filterDto, user);
  }

  @Post()
  createTask(@Body() createTaskDto: CreateTaskDto, @GetUser() user: User) {
    return this.tasksService.createTask(createTaskDto, user);
  }

  @Get('/:id')
  getTask(@Param('id') id: string, @GetUser() user: User) {
    return this.tasksService.getTaskByID(id, user);
  }

  @Patch('/:id/status')
  updateTaskStatus(
    @Param('id') id: string,
    @Body() updateTaskDto: UpdateStatusDto,
    @GetUser() user: User,
  ) {
    const { status } = updateTaskDto;
    this.logger.verbose(
      `User "${
        user.username
      }" creating a updating task status . Data: ${JSON.stringify(
        updateTaskDto,
      )}`,
    );
    return this.tasksService.updateTaskStatus(id, status, user);
  }

  @Delete('/:id')
  deleteTask(@Param('id') id: string, @GetUser() user: User) {
    return this.tasksService.deleteTask(id, user);
  }
}
