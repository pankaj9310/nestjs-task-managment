import { TaskStatus } from './tasks.model';

export class UpdateStatusDto {
  status: TaskStatus;
}
