import {
  ForbiddenException,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { CreateAuthDto } from './dto/create-auth.dto';
import { User } from './entities/auth.entity';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';

@Injectable()
export class AuthService {
  private logger = new Logger('AuthService');
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signUp(createAuthDto: CreateAuthDto): Promise<User> {
    return this.userRepository.createUser(createAuthDto);
  }

  async signIn(createAuthDto: CreateAuthDto): Promise<{ accessToken }> {
    const { username, password } = createAuthDto;
    const user = await this.userRepository.findOne({ username });
    if (user && (await bcrypt.compare(password, user.password))) {
      if (user.is_active) {
        const payload: JwtPayload = {
          username,
        };
        const accessToken = await this.jwtService.sign(payload);
        this.logger.debug(
          `Generated JWT Token with payload ${JSON.stringify(payload)}`,
        );
        return { accessToken };
      } else {
        this.logger.debug('User account is not activated');
        throw new ForbiddenException('Account is not activated');
      }
    }
    throw new UnauthorizedException('Username or password is wrong');
  }
}
