import { Task } from 'src/tasks/task.entity';
import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index({ unique: true })
  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column()
  is_active: boolean;

  @OneToMany((_type) => Task, (task) => task.user, { eager: true })
  task: Task[];
}
