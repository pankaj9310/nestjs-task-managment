import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateAuthDto } from './dto/create-auth.dto';
import { User } from './entities/auth.entity';
import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(createAuthDto: CreateAuthDto): Promise<User> {
    const { username, password } = createAuthDto;
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    const user = this.create({
      username,
      password: hashPassword,
      is_active: true,
    });
    try {
      await this.save(user);
    } catch (error) {
      //duplicate entry handling
      if (error.code === '23505') {
        throw new ConflictException(`Username ${username} already taken`);
      } else {
        throw new InternalServerErrorException();
      }
    }
    return user;
  }
}
