import {
  Controller,
  Get,
  Post,
  Body,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './user.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { User } from './entities/auth.entity';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './get-user.decorator';
import { ConfigService } from '@nestjs/config';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private configService: ConfigService,
  ) {}

  @Post('/signup')
  signUp(@Body(ValidationPipe) createAuthDto: CreateAuthDto): Promise<User> {
    return this.authService.signUp(createAuthDto);
  }

  @Post('/signin')
  signIn(
    @Body(ValidationPipe) createAuthDto: CreateAuthDto,
  ): Promise<{ accessToken }> {
    return this.authService.signIn(createAuthDto);
  }

  @Get()
  @UseGuards(AuthGuard())
  findOne(@GetUser() user: User): User {
    return user;
  }
}
